# slack socket mode  サンプル

Java11 以上で動作します。 
Java8 を利用したい方は `./gradle/wrapper/gradle-wrapper.properties` の 
`distributionUrl=https\://services.gradle.org/distributions/gradle-7.3-bin.zip`
を 
`distributionUrl=https\://services.gradle.org/distributions/gradle-5.6-bin.zip` 
にすれば動作します。

## サンプルアプリの機能
- 作成した Bot が参加しているチャンネルにメッセージ投稿があったら、コンソールにその内容を表示します。
- 作成した Bot にメンションメッセージがあったら、そのメンションメッセージにリアクションをして、スレッドにメッセージを返します。

## サンプルの実行方法
###  (ステップ 0) このリポジトリをダウンロード
ローカルPCにこのリポジトリを クローン 又は ダウンロードします（ダウンロードした場合は ダウンロードした zip ファイルを展開してください）。
![](./images/cloneRepository.png)

###  (ステップ 1) Slack App の初期設定（最初にして最大の難所）
#### Slack App を新規作成 
https://api.slack.com/apps にアクセスして Create New App で アプリを新規作成ボタンを押します。

![](./images/create_slack_app.png)

#### From an app manifest を選択します。
![](./images/fromAnAppManifest.png)

#### アプリをデプロイするワークスペースを選択します。
![](./images/pickWorkspace.png)

#### アプリケーションマニュフェストを入力します。

![](./images/EnterAppManifestBelow.png)

1. デフォルトで入力されている内容を削除します
2. 下に書かれている内容をコピーして貼り付けます
3. 【適切な名前を設定して下さい：例 socketModeTest2021-12】 を書き換えます（２箇所）
4. Nextを押します
5. 内容を確認して Create ボタンを押します。

````
_metadata:
  major_version: 1
  minor_version: 1
display_information:
  name: 【適切な名前を設定して下さい：例 socketModeTest2021-123】
features:
  bot_user:
    display_name: 【適切な名前を設定して下さい：例 socketModeTest2021-123】
    always_online: true
oauth_config:
  scopes:
    bot:
      - channels:history
      - app_mentions:read
      - channels:read
      - users:read
      - chat:write
      - reactions:write
settings:
  event_subscriptions:
    bot_events:
      - message.channels
      - app_mention
  interactivity:
    is_enabled: true
  org_deploy_enabled: false
  socket_mode_enabled: true
  token_rotation_enabled: false
````

![](./images/reviewSummary.png)

![](./images/applicationCreated.png)

#### アプリのインストール

1. 左のメニュー `Install App` を選択。 `Install to Workspace` を選択。

![](./images/insatllApp01.png)

2. 確認ダイアログが出るので確認をして「許可する」を選択します。

![](./images/insatllApp02.png)

3. インストールできました。画面に表示されている `Bot User OAuth Token` は後で使うので、メモしておいて下さい。

![](./images/insatllApp03.png)

4. 左のメニュー `Basic Infomation` の中段 `App-Level Tokens` の `Generate Tokens and Scopes` を押します

![](./images/appToken01.png)

5. Token Name は適当な名前を付けます。 `スコープまたはAPIメソッドによる権限を追加` に `connections:write` を指定して `Generate` ボタンを押します。

![](./images/appToken02.png)

6. `App level token` が発行されました。後で使うので、別途、メモしておいて下さい。

![](./images/appToken03.png)

###  (ステップ 2) 設定 ＆ 実行
#### アプリの実行
ステップ0 でダウンロードしたフォルダ内にある `slackSetting-sample.properties` をコピーして `slackSetting.properties` というファイルを作ります。
`slackSetting.properties` をテキストエディタで開き、「アプリのインストール」でメモをしておいた `Bot User OAuth Token` と `App level token` を設定して保存します。

![](./images/setting.png)

ダウンロードしたフォルダをカレントディレクトリにして `gradlew run` コマンドを入力します。

![](./images/appRunning.png)

#### slack のチャンネルにアプリを参加させる
チャンネルの設定メニューを開き　インテグレーションタブの「アプリを追加する」を選び、作成したアプリを追加します、

![](./images/addBot.png)

チャンネルにメッセージを投稿すると、コンソールに投稿内容が表示されます。
Bot にメンションするとリアクションとスレッド返信がきます。

![](./images/done.png)

## ソース
主要なソースは `./src/main/java/com/mcframe/dx/sample/SlackSocketModeSampleMain.java` です。

## IDEで動かしたい（開発したい）
### Intellij IDEA
メニューバー の File > New > Project from Existing Source > git から取得した `buolde.gradle` を選択

### Eclipse
（知らない）


## リンク
API一覧（≒できること）
https://api.slack.com/methods

Bolt 入門 (ソケットモード)
https://slack.dev/java-slack-sdk/guides/ja/getting-started-with-bolt-socket-mode

「Spring Boot に組み込む」 などのサンプル集
https://github.com/slackapi/java-slack-sdk

## 公式サンプルとの相違点
Slack SDK for Java　Bolt 入門 (ソケットモード) https://slack.dev/java-slack-sdk/guides/ja/getting-started-with-bolt-socket-mode 
とこのサンプルの主な相違点は以下のとおりです。

- slack アプリの初期設定を App Manifest ベースにしています
- slack コマンド を用いたサンプル実装ではなく、メッセージを基本としたサンプルに変更しています
- token を環境変数にセットするのではなく `slackSetting.properties` に設定するようにした
    - 目的：実行環境（特に IDE 使用時と非使用時）に設定方法が異なるので統一したかった
    - 主な変更箇所： `build.gradle` の でファイルを読み込み環境変数をセットするコードを書いています
- logger の導入
    - SLF4j を導入しています。 出力バインディングは `slf4j-simple` を使用しています
    - 主な変更箇所： `build.gradle` で依存を追記
- デフォルトのログレベルを debug に指定
    - 主な変更箇所： `build.gradle` の RUN タスクにて設定
- 一部の slack との通信処理を `SlackHelper` クラスに実装
    - サンプルの可読性を上げるために slack との通信手続きを `SlackHelper` クラス に委譲しています。
