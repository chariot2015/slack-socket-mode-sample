package com.mcframe.dx.sample;

import com.slack.api.bolt.context.builtin.EventContext;
import com.slack.api.methods.SlackApiException;
import com.slack.api.methods.request.conversations.ConversationsInfoRequest;
import com.slack.api.methods.response.chat.ChatPostMessageResponse;
import com.slack.api.methods.response.conversations.ConversationsInfoResponse;
import com.slack.api.methods.response.reactions.ReactionsAddResponse;
import com.slack.api.methods.response.users.UsersInfoResponse;
import com.slack.api.model.Conversation;
import com.slack.api.model.event.AppMentionEvent;
import com.slack.api.model.event.MessageBotEvent;
import com.slack.api.model.event.MessageEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;

public class SlackHelper {
    private static final Logger logger = LoggerFactory.getLogger(SlackSocketModeSampleMain.class);
    private final EventContext context;

    public SlackHelper(EventContext context) {
        this.context = context;
    }

    public static OffsetDateTime toOffsetDateTime(String slackTsString) {
        if (slackTsString == null) return null;
        final String[] split = slackTsString.split("\\.");
        long epochSecond = Long.parseLong(split[0]);
        long nanoAdjustment = (split.length > 1) ? Long.parseLong(split[1]) : 0L;
        Instant instant = Instant.ofEpochSecond(epochSecond, nanoAdjustment);
        return OffsetDateTime.ofInstant(instant, ZoneId.systemDefault());
    }

    public static boolean isThreadMessage(MessageEvent messageEvent) {
        if (messageEvent == null) return false;
        return messageEvent.getThreadTs() == null;
    }

    public UsersInfoResponse getUserInfo(String userId) {
        final UsersInfoResponse usersInfoResponse;
        try {
            usersInfoResponse = context.client().usersInfo(usersInfoRequestBuilder -> usersInfoRequestBuilder.user(userId));
        } catch (IOException | SlackApiException e) {
            throw new RuntimeException(e);
        }
        if (usersInfoResponse.isOk()) return usersInfoResponse;
        // 権限が不足している場合などは例外にはならず、 isOk() == false になります。失敗した内容はログを確認してください。
        logger.warn(usersInfoResponse.toString());
        return null;
    }

    public String getUserName(String userId) {
        final UsersInfoResponse userInfo;
        try {
            userInfo = getUserInfo(userId);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return userInfo == null ? null : userInfo.getUser().getName();
    }

    public Conversation getChannelInfo(String channelId) {
        final ConversationsInfoResponse conversationsInfoResponse;
        try {
            conversationsInfoResponse = context.client().conversationsInfo(ConversationsInfoRequest.builder().channel(channelId).build());
        } catch (IOException | SlackApiException e) {
            throw new RuntimeException(e);
        }
        if (conversationsInfoResponse.isOk()) return conversationsInfoResponse.getChannel();
        // 権限が不足している場合などは例外にはならず、 isOk() == false になります。失敗した内容はログを確認してください。
        logger.warn(conversationsInfoResponse.toString());
        return null;
    }

    public String getChannelName(String channelId) {
        final Conversation channelInfo = getChannelInfo(channelId);
        return channelInfo == null ? null : channelInfo.getName();
    }

    public void reaction(MessageEvent messageEvent, String reaction) {
        reaction(messageEvent.getChannel(), messageEvent.getEventTs(), reaction);
    }

    public void reaction(AppMentionEvent messageEvent, String reaction) {
        reaction(messageEvent.getChannel(), messageEvent.getEventTs(), reaction);
    }

    private void reaction(String channelId, String eventTs, String messageText) {
        ReactionsAddResponse reactionsAddResponse;
        try {
            reactionsAddResponse = context.client().reactionsAdd(reactionsAddRequestBuilder -> reactionsAddRequestBuilder
                    .channel(channelId)
                    .timestamp(eventTs) //  slack は channel と timestamp でメッセージを特定します
                    .name(messageText)
            );
        } catch (IOException | SlackApiException e) {
            throw new RuntimeException(e);
        }
        if (reactionsAddResponse.isOk()) return;
        // 権限が不足している場合などは例外にはならず、 isOk() == false になります。失敗した内容はログを確認してください。
        logger.info(reactionsAddResponse.toString());
    }

    public void threadMessage(MessageEvent messageEvent, String messageText) {
        threadMessage(messageEvent.getChannel(), messageEvent.getEventTs(), messageText);
    }

    public void threadMessage(AppMentionEvent messageEvent, String messageText) {
        threadMessage(messageEvent.getChannel(), messageEvent.getEventTs(), messageText);
    }

    public void threadMessage(MessageBotEvent messageEvent, String messageText) {
        threadMessage(messageEvent.getChannel(), messageEvent.getEventTs(), messageText);
    }

    private void threadMessage(String channelId, String eventTs, String messageText) {
        ChatPostMessageResponse chatPostMessageResponse;
        try {
            chatPostMessageResponse = context.client().chatPostMessage(chatPostMessageRequestBuilder -> chatPostMessageRequestBuilder
                    .channel(channelId)
                    .threadTs(eventTs)
                    .text(messageText)
            );
        } catch (IOException | SlackApiException e) {
            throw new RuntimeException(e);
        }
        if (chatPostMessageResponse.isOk()) return;
        // 権限が不足している場合などは例外にはならず、 isOk() == false になります。失敗した内容はログを確認してください。
        logger.warn(chatPostMessageResponse.toString());

    }

}
