package com.mcframe.dx.sample;

import com.slack.api.bolt.App;
import com.slack.api.bolt.socket_mode.SocketModeApp;
import com.slack.api.model.event.AppMentionEvent;
import com.slack.api.model.event.MessageEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

public class SlackSocketModeSampleMain {

    public static void main(String... args) throws Exception {
        logger.debug("BOT_TOKEN: " + toMaskedToken(System.getenv().get("SLACK_BOT_TOKEN")));
        logger.debug("APP_TOKEN: " + toMaskedToken(System.getenv().get("SLACK_APP_TOKEN")));
        App app = new App();
        app.event(MessageEvent.class, (event, context) -> {
            SlackHelper helper = new SlackHelper(context);
            MessageEvent messageEvent = event.getEvent();
            // サンプル：ログ出力
            OffsetDateTime eventAt = SlackHelper.toOffsetDateTime(messageEvent.getEventTs());
            String userId = messageEvent.getUser();
            String userName = helper.getUserName(userId);
            String channelId = messageEvent.getChannel();
            String channelName = helper.getChannelName(channelId);
            String messageBody = messageEvent.getText();
            logger.info(String.format("\n%s\t%s\t%s\t%s "
                    , logDatetimeFormatter.format(eventAt)
                    , channelName, userName, messageBody));
            return context.ack();
        });
        app.event(AppMentionEvent.class, (event, context) -> {
            final AppMentionEvent appMentionEvent = event.getEvent();
            SlackHelper helper = new SlackHelper(context);
            // リアクションする
            helper.reaction(appMentionEvent, "+1");
            // スレッド返信する
            helper.threadMessage(appMentionEvent, "Enjoy Slack!! :slack:");

            return context.ack();
        });
        new SocketModeApp(app).start();
    }

    private static final Logger logger = LoggerFactory.getLogger(SlackSocketModeSampleMain.class);
    private static final DateTimeFormatter logDatetimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");

    private static String toMaskedToken(String token) {
        final int numberOfCharactersToDisplay = 6;
        return Optional.ofNullable(token)
                .map(aToken -> aToken.length() > numberOfCharactersToDisplay
                        ? aToken.substring(0, numberOfCharactersToDisplay) + "***"
                        : aToken)
                .orElse(" [not set]");
    }
}
